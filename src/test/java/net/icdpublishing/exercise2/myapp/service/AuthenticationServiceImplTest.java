package net.icdpublishing.exercise2.myapp.service;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import net.icdpublishing.exercise2.myapp.customers.dao.CustomerNotFoundException;

public class AuthenticationServiceImplTest {
	
	private AuthenticationService service;
	
	@Before
	public void setUp(){
		service = new AuthenticationServiceImpl();
	}

	@Test
	public void testIfUserIsValidCustomer() {
		boolean isValidCustomer = service.authenticate("sally.smith@192.com");
		assertTrue(isValidCustomer);
	}
	
	@Test(expected=CustomerNotFoundException.class)
	public void testIfUserIsNotValidCustomerAndCustomerNotFoundExceptionIsThrown() {
		service.authenticate("abc.xyz@g.com");
	}

}
