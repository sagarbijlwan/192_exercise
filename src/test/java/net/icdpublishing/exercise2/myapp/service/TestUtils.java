package net.icdpublishing.exercise2.myapp.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import net.icdpublishing.exercise2.myapp.customers.domain.Customer;
import net.icdpublishing.exercise2.myapp.customers.domain.CustomerType;
import net.icdpublishing.exercise2.searchengine.domain.Address;
import net.icdpublishing.exercise2.searchengine.domain.Person;
import net.icdpublishing.exercise2.searchengine.domain.Record;
import net.icdpublishing.exercise2.searchengine.domain.SourceType;

public final class TestUtils {
	
	public static Customer populateCustomer(final String emailId, final String forename, final String surname, final CustomerType custtype ){
        Customer customer = new Customer();
        customer.setEmailAddress(emailId);
        customer.setForename(forename);
        customer.setSurname(surname);
        customer.setCustomType(custtype);
        return customer;
	}
	
	public static Customer populateDummyCustomer(){
        Customer customer = new Customer();
        customer.setEmailAddress("abc.xyz@z.com");
        customer.setForename("Abc");
        customer.setSurname("Xyz");
        customer.setCustomType(CustomerType.PREMIUM);
        return customer;
	}
	
	public static Collection<Record> popluateSingleRecordWithAllSources(){
		Collection<Record> dummySet = new ArrayList<Record>();
	    Person dummyperson = createPerson("George", "Vince", "Dunhill", "0779942223");
	    Address dummyaddress = createPersonAddress("1","gu1 4ny", "dapdune way", "guildford");
	    dummyperson.setAddress(dummyaddress);
	    Set<SourceType> dummySources = new HashSet<SourceType>();
	    dummySources.add(SourceType.BT);
	    dummySources.add(SourceType.DNB);
	    dummySources.add(SourceType.ELECTORAL_ROLL);
	    Record record = new Record(dummyperson, dummySources);
	    dummySet.add(record);
	    return Collections.unmodifiableCollection(dummySet);
	}
	
	public static Collection<Record> popluateSingleRecordWithJustBTAsSource(){
		Collection<Record> dummySet = new ArrayList<Record>();
	    Person dummyperson = createPerson("Rupert", "James", "Prince", "07259859423");
	    Address dummyaddress = createPersonAddress("3","gu12 3eq", "forrest way", "fleet");
	    dummyperson.setAddress(dummyaddress);
	    Set<SourceType> dummySources = new HashSet<SourceType>();
	    dummySources.add(SourceType.BT);
	    Record record = new Record(dummyperson, dummySources);
	    dummySet.add(record);
	    return Collections.unmodifiableCollection(dummySet);
	}
	
	public static Collection<Record> popluateMultipleRecords(){
		Collection<Record> dummySet = new ArrayList<Record>();
		
	    Person dummyperson1 = createPerson("Rupert", "James", "Prince", "07259859423");
	    Address dummyaddress1 = createPersonAddress("3","gu12 3eq", "forrest way", "fleet");
	    dummyperson1.setAddress(dummyaddress1);
	    Set<SourceType> dummySources1 = new HashSet<SourceType>();
	    dummySources1.add(SourceType.BT);
	    Record record1 = new Record(dummyperson1, dummySources1);
	    dummySet.add(record1);
	    
	    Person dummyperson2 = createPerson("George", "Vince", "Dunhill", "0779942223");
	    Address dummyaddress2 = createPersonAddress("1","gu1 4ny", "dapdune way", "guildford");
	    dummyperson2.setAddress(dummyaddress2);
	    Set<SourceType> dummySources2 = new HashSet<SourceType>();
	    dummySources2.add(SourceType.BT);
	    dummySources2.add(SourceType.DNB);
	    dummySources2.add(SourceType.ELECTORAL_ROLL);
	    Record record2 = new Record(dummyperson2, dummySources2);
	    dummySet.add(record2);
	    
	    Person dummyperson3 = createPerson("Annie", "Peter", "Clearwater", "07893532552");
	    Address dummyaddress3 = createPersonAddress("4","gu2 1my", "downhill road", "onslow");
	    dummyperson3.setAddress(dummyaddress3);
	    Set<SourceType> dummySources3 = new HashSet<SourceType>();
	    dummySources3.add(SourceType.BT);
	    dummySources3.add(SourceType.DNB);
	    dummySources3.add(SourceType.ELECTORAL_ROLL);
	    Record record3 = new Record(dummyperson3, dummySources3);
	    dummySet.add(record3);
	    
	    return Collections.unmodifiableCollection(dummySet);
	}

	private static Address createPersonAddress(final String buildNo, final String postCode, final String street, final String town) {
		Address dummyaddress = new Address();
	    dummyaddress.setBuildnumber(buildNo);
	    dummyaddress.setPostcode(postCode);
	    dummyaddress.setStreet(street);
	    dummyaddress.setTown(town);
		return dummyaddress;
	}

	private static Person createPerson(final String forename, final String middlename, final String surname, final String telephone) {
		Person dummyperson = new Person();
		dummyperson.setForename(forename);
	    dummyperson.setMiddlename(middlename);
	    dummyperson.setSurname(surname);
	    dummyperson.setTelephone(telephone);
	    return dummyperson;
	}

}
