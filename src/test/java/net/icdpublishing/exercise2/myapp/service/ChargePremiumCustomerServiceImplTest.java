package net.icdpublishing.exercise2.myapp.service;

import static org.mockito.Mockito.times;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;

import net.icdpublishing.exercise2.myapp.charging.services.ChargingService;

public class ChargePremiumCustomerServiceImplTest {
	
	@Mock
	private ChargingService chargingService;

	private ProcessResultsService service;
	
	@Before
	public void setUp() throws Exception{
		service = new ChargePremiumCustomerServiceImpl();
		MockitoAnnotations.initMocks(this);
		ReflectionTestUtils.setField(service, "chargingService", chargingService);
	}

	@Test
	public void testChargingServiceIsCalledWhenSourcesMoreThanBTArePresent() {
		service.processResults(TestUtils.populateDummyCustomer(), TestUtils.popluateSingleRecordWithAllSources());
		Mockito.verify(chargingService, times(1)).charge("abc.xyz@z.com", 1);
	}
	
	@Test
	public void testChargingServiceIsNotCalledWhenSourcesIsJustBT() {
		service.processResults(TestUtils.populateDummyCustomer(), TestUtils.popluateSingleRecordWithJustBTAsSource());
		Mockito.verify(chargingService, times(0)).charge("abc.xyz@z.com", 1);
	}
	
	@Test(expected = RuntimeException.class)
	public void testRuExceptnTimeExceptionIsThrown() {
		service.processResults(null,  TestUtils.popluateSingleRecordWithAllSources());
	}
	
}
