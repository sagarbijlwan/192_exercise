package net.icdpublishing.exercise2.myapp.service;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

import net.icdpublishing.exercise2.searchengine.domain.Record;
import net.icdpublishing.exercise2.searchengine.domain.SourceType;

public class FilterResultsOnCustomerTypeServiceImplTest {
	
	private ProcessResultsService service;
	
	@Before
	public void setUp(){
		service = new FilterResultsOnCustomerTypeServiceImpl();
	}

	@Test
	public void testFilteredRecordsAreReturnedWithBTAsSourceType() {
		Collection<Record> result = service.processResults(TestUtils.populateDummyCustomer(), TestUtils.popluateMultipleRecords());
		assertEquals(result.size(), 1);
		List<Record> resultSet = new LinkedList<>(result);
		assertEquals(resultSet.get(0).getSourceTypes().size(), 1);
		assertEquals(resultSet.get(0).getSourceTypes().iterator().next(), SourceType.BT);
	}
	
	@Test
	public void testNoRecordIsReturnedWhenMultipleSourceType() {
		Collection<Record> result = service.processResults(TestUtils.populateDummyCustomer(), TestUtils.popluateSingleRecordWithAllSources());
		assertEquals(result.size(), 0);
	}

}
