package net.icdpublishing.exercise2.myapp;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;

import java.util.Collection;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;

import net.icdpublishing.exercise2.myapp.customers.domain.Customer;
import net.icdpublishing.exercise2.myapp.customers.domain.CustomerType;
import net.icdpublishing.exercise2.myapp.service.AuthenticationService;
import net.icdpublishing.exercise2.myapp.service.ProcessResultsService;
import net.icdpublishing.exercise2.myapp.service.TestUtils;
import net.icdpublishing.exercise2.myapp.util.ProcessRecordUtil;
import net.icdpublishing.exercise2.searchengine.domain.Record;
import net.icdpublishing.exercise2.searchengine.domain.SourceType;
import net.icdpublishing.exercise2.searchengine.requests.SimpleSurnameAndPostcodeQuery;
import net.icdpublishing.exercise2.searchengine.services.SearchEngineRetrievalService;

public class MySearchControllerTest {
	
    private MySearchController controller;
    
    @Mock
    private AuthenticationService authenticationService;
	
    @Mock
	private SearchEngineRetrievalService retrievalService;

    @Mock
	private ProcessResultsService processResultService;
    

    @Before
    public void setUp() throws Exception {
    	controller = new MySearchController();
    	MockitoAnnotations.initMocks(this);
    	ReflectionTestUtils.setField(controller, "authenticationService", authenticationService);
    	ReflectionTestUtils.setField(controller, "retrievalService", retrievalService);
    	ReflectionTestUtils.setField(controller, "processResultService", processResultService);
    }

    @Test
    public void testSearchRequestIsCompletedAndPremiumCustomerIsCharged() {
    	SimpleSurnameAndPostcodeQuery query = new SimpleSurnameAndPostcodeQuery("Jones", "ec2 2aq");
    	Customer testCustomer = TestUtils.populateCustomer("sally.smith@192.com", "sally", "smith", CustomerType.PREMIUM);
    	Collection<Record> searchResults = TestUtils.popluateMultipleRecords();
    	ProcessRecordUtil.sortRecordsOnSurName(searchResults);
    	SearchRequest request = new SearchRequest(query, testCustomer);
    	Mockito.when(authenticationService.authenticate(testCustomer.getEmailAddress())).thenReturn(true);
    	Mockito.when(retrievalService.search(request.getQuery())).thenReturn(searchResults);
    	Collection<Record> actualResult = controller.handleRequest(request);
    	Mockito.verify(authenticationService, times(1)).authenticate(testCustomer.getEmailAddress());
    	Mockito.verify(retrievalService, times(1)).search(request.getQuery());
    	Mockito.verify(processResultService, times(1)).processResults(testCustomer, searchResults);
    	assertEquals(actualResult, searchResults);
    	assertEquals(actualResult.size(), 3);
    	assertEquals(actualResult.iterator().next().getPerson().getSurname(), searchResults.iterator().next().getPerson().getSurname());
    }
    
    @Test
    public void testSearchRequestIsCompletedForNonPremiumCustomerAndOnlyBTSourceIsReturned() {
    	SimpleSurnameAndPostcodeQuery query = new SimpleSurnameAndPostcodeQuery("Jones", "ec2 2aq");
    	Customer testCustomer = TestUtils.populateCustomer("sally.smith@192.com", "sally", "smith", CustomerType.NON_PAYING);
    	Collection<Record> searchResults = ProcessRecordUtil.filterRecordWhenSourceTypeIsJustBT(TestUtils.popluateMultipleRecords());
    	SearchRequest request = new SearchRequest(query, testCustomer);
    	Mockito.when(authenticationService.authenticate(testCustomer.getEmailAddress())).thenReturn(true);
    	Mockito.when(retrievalService.search(request.getQuery())).thenReturn(searchResults);
    	Collection<Record> actualResult = controller.handleRequest(request);
    	Mockito.verify(authenticationService, times(1)).authenticate(testCustomer.getEmailAddress());
    	Mockito.verify(retrievalService, times(1)).search(request.getQuery());
    	Mockito.verify(processResultService, times(1)).processResults(testCustomer, searchResults);
    	assertEquals(actualResult.size(), 1);
    	assertEquals(actualResult.iterator().next().getSourceTypes().iterator().next(), SourceType.BT);
    }
    
    @Test 
    public void testForUnregisteredCustomerSearchRequestIsDiscardedAndEmptyCollectionIsReturned() {
    	SimpleSurnameAndPostcodeQuery query = new SimpleSurnameAndPostcodeQuery("Jones", "ec2 2aq");
    	Customer unregisteredCustomer = TestUtils.populateCustomer("aad.afa@nb.com", "sally", "smith", CustomerType.NON_PAYING);
    	SearchRequest request = new SearchRequest(query, unregisteredCustomer);
    	Mockito.when(authenticationService.authenticate("aad.afa@nb.com")).thenReturn(false);
    	Collection<Record> actualResult = controller.handleRequest(request);
    	assertEquals(actualResult, CollectionUtils.EMPTY_COLLECTION);
    	Mockito.verify(retrievalService, times(0)).search(request.getQuery());
    }
}
