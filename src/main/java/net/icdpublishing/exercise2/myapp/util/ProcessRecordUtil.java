package net.icdpublishing.exercise2.myapp.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;

import net.icdpublishing.exercise2.searchengine.domain.Record;
import net.icdpublishing.exercise2.searchengine.domain.SourceType;

/**
 * The Utility class for processing the records.
 *
 */
public final class ProcessRecordUtil {
	
	private ProcessRecordUtil() {
		
	}
	
	private static final class SurNameComparator implements Comparator<Record> {
		// Assuming each person in the records has surname.
		@Override
		public int compare(Record o1, Record o2) {
			return o1.getPerson().getSurname().compareTo(o2.getPerson().getSurname());
		}
		
	}
	
	public static Collection<Record> filterRecordWhenSourceTypeIsJustBT(Collection<Record> resultSet) {
		Predicate<Record> predicate = new Predicate<Record>() {
			
			@Override
			public boolean apply(Record result) {
				return  (isSourceTypeSingle(result) && isSourceTypeBT(result));
			}

			private boolean isSourceTypeBT(Record result) {
				return result.getSourceTypes().contains(SourceType.BT);
			}

			private boolean isSourceTypeSingle(Record result) {
				return result.getSourceTypes().size() == 1;
			}
		};
		return Collections2.filter(resultSet, predicate);
	}
	
	public static void sortRecordsOnSurName(Collection<Record> resultSet) {
			List<Record> resultSetList = new ArrayList<Record>(resultSet);
			Collections.sort(resultSetList, new SurNameComparator());
	}

}
