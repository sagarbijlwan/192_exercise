package net.icdpublishing.exercise2.myapp;

import java.util.Collection;

import net.icdpublishing.exercise2.myapp.customers.domain.Customer;
import net.icdpublishing.exercise2.myapp.customers.domain.CustomerType;
import net.icdpublishing.exercise2.searchengine.domain.Record;
import net.icdpublishing.exercise2.searchengine.loader.DataLoader;
import net.icdpublishing.exercise2.searchengine.requests.SimpleSurnameAndPostcodeQuery;
import net.icdpublishing.exercise2.searchengine.services.DummyRetrievalServiceImpl;
import net.icdpublishing.exercise2.searchengine.services.SearchEngineRetrievalService;

public class DemoRun {

	public static void main(String[] args) {
		//"harry.lang@192.com"
		 Customer c = new Customer();
	        c.setEmailAddress("asda");
	        c.setForename("afsag");
	        c.setSurname("dgs");
	        c.setCustomType(CustomerType.PREMIUM);
		// TODO Auto-generated method stub
		SearchEngineRetrievalService service = new DummyRetrievalServiceImpl(new DataLoader());
		MySearchController controller = new MySearchController(service);
		SimpleSurnameAndPostcodeQuery query = new SimpleSurnameAndPostcodeQuery("Smith", "sw6 2bq");
		SearchRequest request = new SearchRequest(query, c);
		Collection<Record> resultSet = controller.handleRequest(request);
		resultSet.forEach(result -> System.out.println(result.getPerson().getForename() + " " + result.getPerson().getSurname()));
	}

}
