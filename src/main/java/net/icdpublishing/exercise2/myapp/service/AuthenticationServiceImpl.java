package net.icdpublishing.exercise2.myapp.service;

import net.icdpublishing.exercise2.myapp.customers.dao.CustomerDao;
import net.icdpublishing.exercise2.myapp.customers.dao.CustomerNotFoundException;
import net.icdpublishing.exercise2.myapp.customers.dao.HardcodedListOfCustomersImpl;
import net.icdpublishing.exercise2.myapp.customers.domain.Customer;

/** 
 * The Service implementation where user is authenticated.
 *
 */
public class AuthenticationServiceImpl implements AuthenticationService {
	
	private CustomerDao customerDAO;

	@Override
	public boolean authenticate(final String customerEmailAddress) throws CustomerNotFoundException{
		boolean isUserAuthentic = false;
		// If customer exist in DB then it is valid customer.
		customerDAO = new HardcodedListOfCustomersImpl();
		Customer customer = customerDAO.findCustomerByEmailAddress(customerEmailAddress);
		if(customer != null){
			isUserAuthentic = true;
		}
		return isUserAuthentic;
	}

}
