package net.icdpublishing.exercise2.myapp.service;

import net.icdpublishing.exercise2.myapp.customers.dao.CustomerNotFoundException;

/**
 * The Authentication service to authenticate a user/customer.
 *
 */

public interface AuthenticationService {
	
	boolean authenticate(String customerEmailAddress) throws CustomerNotFoundException;

}
