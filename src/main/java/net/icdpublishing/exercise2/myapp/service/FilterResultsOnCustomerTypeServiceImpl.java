package net.icdpublishing.exercise2.myapp.service;

import java.util.Collection;

import net.icdpublishing.exercise2.myapp.customers.domain.Customer;
import net.icdpublishing.exercise2.myapp.util.ProcessRecordUtil;
import net.icdpublishing.exercise2.searchengine.domain.Record;

/**
 * The Service will filter results returned from search engine based on customer type.
 */
public class FilterResultsOnCustomerTypeServiceImpl implements ProcessResultsService {

	@Override
	public Collection<Record> processResults(Customer customer, Collection<Record> resultSet) throws RuntimeException{
		return ProcessRecordUtil.filterRecordWhenSourceTypeIsJustBT(resultSet);
	}


}
