package net.icdpublishing.exercise2.myapp.service;

import java.util.Collection;

import net.icdpublishing.exercise2.myapp.customers.domain.Customer;
import net.icdpublishing.exercise2.searchengine.domain.Record;

/**
 * The Service will assist controller to filter records fetched from search engine.
 * 
 * 
 *
 */
public interface ProcessResultsService {
	
	Collection<Record> processResults(Customer customer, Collection<Record> resultSet) throws RuntimeException;
}
