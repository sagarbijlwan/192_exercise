package net.icdpublishing.exercise2.myapp.service;

import java.util.Collection;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;

import net.icdpublishing.exercise2.myapp.charging.services.ChargingService;
import net.icdpublishing.exercise2.myapp.charging.services.ImaginaryChargingService;
import net.icdpublishing.exercise2.myapp.customers.domain.Customer;
import net.icdpublishing.exercise2.myapp.util.ProcessRecordUtil;
import net.icdpublishing.exercise2.searchengine.domain.Record;

/**
 * The Service will charge the service for premium customer based on number of
 * records returned.
 *
 */
public class ChargePremiumCustomerServiceImpl implements ProcessResultsService {

	private static final Logger LOGGER = Logger.getLogger(ChargePremiumCustomerServiceImpl.class);

	private ChargingService chargingService;

	@SuppressWarnings("unchecked")
	@Override
	public Collection<Record> processResults(Customer customer, Collection<Record> resultSet) throws RuntimeException {
		// Collect all the records where source type is just BT.
		Collection<Record> filteredResultSetWhereSourceTypeIsJustBT = ProcessRecordUtil
				.filterRecordWhenSourceTypeIsJustBT(resultSet);
		// Collect all the records where source type is just not BT.
		Collection<Record> filteredResultSetWhereSourceTypeIsNotJustBT = CollectionUtils.disjunction(resultSet,
				filteredResultSetWhereSourceTypeIsJustBT);
		// If there are records where source type is just not BT then call the
		// charging service.
		if (CollectionUtils.isNotEmpty(filteredResultSetWhereSourceTypeIsNotJustBT)) {
				// Calling legacy charging service with the assumption that
				// charging service has the logic to deduct credits.
				// First argument is the email Id of the customer.
				// Second is the number of records/credits for which customer should be
				// charged.
				//chargingService = new ImaginaryChargingService();
				chargingService.charge(customer.getEmailAddress(), filteredResultSetWhereSourceTypeIsNotJustBT.size());
		} else {
			LOGGER.info(" Customer can't be charged as no record found other than BT as source type");
		}

		return resultSet;
	}

}
