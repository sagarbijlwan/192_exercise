package net.icdpublishing.exercise2.myapp;

import java.util.Collection;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;

import net.icdpublishing.exercise2.myapp.charging.ChargingException;
import net.icdpublishing.exercise2.myapp.customers.dao.CustomerNotFoundException;
import net.icdpublishing.exercise2.myapp.customers.domain.Customer;
import net.icdpublishing.exercise2.myapp.customers.domain.CustomerType;
import net.icdpublishing.exercise2.myapp.service.AuthenticationService;
import net.icdpublishing.exercise2.myapp.service.AuthenticationServiceImpl;
import net.icdpublishing.exercise2.myapp.service.ChargePremiumCustomerServiceImpl;
import net.icdpublishing.exercise2.myapp.service.FilterResultsOnCustomerTypeServiceImpl;
import net.icdpublishing.exercise2.myapp.service.ProcessResultsService;
import net.icdpublishing.exercise2.myapp.util.ProcessRecordUtil;
import net.icdpublishing.exercise2.searchengine.domain.Record;
import net.icdpublishing.exercise2.searchengine.requests.SimpleSurnameAndPostcodeQuery;
import net.icdpublishing.exercise2.searchengine.services.SearchEngineRetrievalService;

public class MySearchController {
	
	private static final Logger LOGGER = Logger.getLogger(MySearchController.class);
	
	private AuthenticationService authenticationService;
	
	private SearchEngineRetrievalService retrievalService;

	private ProcessResultsService processResultService;
	
	public MySearchController() {
	}

	public MySearchController(SearchEngineRetrievalService retrievalService) {
		this.retrievalService = retrievalService;
	}

	public Collection<Record> handleRequest(SearchRequest request) {
		
		try {
			// Requirement 3
			// Each search request needs to be authenticated.
			//authenticationService = new AuthenticationServiceImpl();
			if(LOGGER.isDebugEnabled()){
				LOGGER.debug(" Authentication Service will be called before search ");
			}
			if (authenticationService.authenticate(request.getCustomer().getEmailAddress())) {
				Collection<Record> resultSet = getResults(request.getQuery());
				// Requirement 1
				// IF customer is non paying then customer should only see BT as
				// the
				// exclusive source type.
				// Premium customers can view results with any source type.
				// Call for filtering of resultset must be made in case of Non
				// paying
				// customer.
				if (CustomerType.NON_PAYING.equals(request.getCustomer().getCustomType())) {
					 Collection<Record> filteredResultSet = filterResultBasedOnCustomer(request.getCustomer(), resultSet);
					 if(CollectionUtils.isNotEmpty(filteredResultSet)){
						 ProcessRecordUtil.sortRecordsOnSurName(filteredResultSet);
						 return filteredResultSet;
					 }
				} else {
					// Requirement 2
					// IF customer is premium type then system should charge the
					// customer.
					chargeCustomer(request.getCustomer(), resultSet);
					ProcessRecordUtil.sortRecordsOnSurName(resultSet);
				}
				return resultSet;
			}
		} catch (CustomerNotFoundException  | ChargingException ce) {
			LOGGER.info(" Exception while processing. Exception is -> " + ce.getMessage());
		}
		return CollectionUtils.EMPTY_COLLECTION;
	}

	private Collection<Record> getResults(SimpleSurnameAndPostcodeQuery query) {
		return retrievalService.search(query);
	}

	private Collection<Record> filterResultBasedOnCustomer(final Customer customer,
			final Collection<Record> resultSet) {
		//processResultService = new FilterResultsOnCustomerTypeServiceImpl();
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug(" Filter Service will be called for Non paying Customer ");
		}
		return processResultService.processResults(customer, resultSet);
	}

	private void chargeCustomer(final Customer customer, final Collection<Record> resultSet) {
		//processResultService = new ChargePremiumCustomerServiceImpl();
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug(" Premium customer will be charged credits for the search ");
		}
		processResultService.processResults(customer, resultSet);
	}
}