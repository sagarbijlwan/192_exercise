1. Search Service will give at least one Record.

2. Logger is enabled for the application and statements are logged at some points in the application.

3. Though the application can be wired with Spring framework. However since the legacy applications also don't seem to be spring enabled at this point. So newly added features are also added without spring.

4. At some points newly created services are instantiated with new operator. But these points are commented for now. The instantiation statements are there for better readability.

5. A DemoRun is the main class added for the application to run. This can be run after instantiation LOCs mentioned in above point are  uncommented.

6. Assumption is made that credit deducting service will take care of deducting credits and hence no logic is developed in this regard.

7. For Testing Mockito framemwork and Spring test is employed.